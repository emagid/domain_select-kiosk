<?php



/**
 * encrypt a message or value.  useful for hiding the actual ID number of any given record in the database.  numeric ID numbers should never be visible in HTML or XML source code!
 * do not confuse with {@link encode()}
 * <br><br>Example:
 * <code>
 * <?php
 * $person_id = 12345;
 * $person_ide = encrypt($person_id,'person');
 * // $person_ide: iYq8okuGw9a
 * $temp = decrypt($person_ide,'person');
 * // $temp: 12345
 * ?>
 * </code>
 * @version 2.1
 * @param string $message the message or value to encrypt; must only contain visible characters and spaces (i.e. POSIX [:print:])
 * @param string $key the private key, typically the name of the variable is used as the private key
 * @return string returns an encrypted message
 * @see decrypt()
 */
function encrypt($message, $key=31337) {
    global $sky_encryption_key;
    if (!$key) $key = 31337;
    $temp = $key . $sky_encryption_key;
    $source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if(strlen($temp) < 16){
        $temp.= substr($source, 0, 16-strlen($temp));
    }
    if (strlen($temp) > 16) $key = substr(strrev($key),0, 16 - strlen($sky_encryption_key)) . $sky_encryption_key;
    else $key = $temp;
    $iv_size = mcrypt_get_iv_size(MCRYPT_XTEA, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $enc = bin2hex(mcrypt_encrypt(MCRYPT_XTEA, $key, $message, MCRYPT_MODE_ECB, $iv));
    return my_base_convert('1'.$enc,16,62);
}//function

function encryptFn($key = null) {
    return function($message) use($key) {
        return encrypt($message, $key);
    };
}


/**
 * Add ide for all id's in the model
 */
function addIde($arr , $name = ''){

    if (isset($arr['id'])) {
        $arr['ide'] = encrypt($arr['id'], $name);
    }

    foreach ($arr as $key => $value) {
        if (endsWith($key,'_id') && !isset($arr[$key . 'e'])){
            $table = substr($key, 0, -3);

            $arr[$key.'e'] = encrypt($value, $table);
        }
    }


    return $arr  ;
}



/**
 * decrypt an encrypted message or value
 * <br><br>Example:
 * <code>
 * <?php
 * $person_id = 12345;
 * $person_ide = encrypt($person_id,'person');
 * // $person_ide: iYq8okuGw9a
 * $temp = decrypt($person_ide,'person');
 * // $temp: 12345
 * ?>
 * </code>
 * @version 2.1
 * @param string $encrypted_message the encrypted message or value
 * @param string $key the private key, typically the name of the variable is used as the private key
 * @return string returns a decrypted message or false when unsuccessful
 * @see encrypt()
 */
function decrypt($encrypted_message, $key=31337, $force_return  = true ) {
    global $sky_encryption_key;
    if (!$key) $key = 31337;
    $temp = $key . $sky_encryption_key;
    $source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if(strlen($temp) < 16){
        $temp.= substr($source, 0, 16-strlen($temp));
    }
    if (strlen($temp) > 16) $key = substr(strrev($key),0, 16 - strlen($sky_encryption_key)) . $sky_encryption_key;
    else $key = $temp;
    $encrypted_message = my_base_convert($encrypted_message,62,16);
    $encrypted_message = substr($encrypted_message,1);
    $iv_size = mcrypt_get_iv_size(MCRYPT_XTEA, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $temp = trim(mcrypt_decrypt(MCRYPT_XTEA, $key, pack("H*", $encrypted_message), MCRYPT_MODE_ECB, $iv));
    if ($force_return  || ctype_alnum($temp)) return $temp;
    else return false;
}//function

function decryptFn($key = null) {
    return function($message) use($key) {
        return decrypt($message, $key);
    };
}


/**
 * convert a number from any base to any other base (max base 62)
 * currently there is no sanity check on input values
 * @version 1.0
 * @param string $numstring case-sensitive alphanumeric input representing a number of any base (base2-base62). lowercase must be used for base11-base36.
 * @param integer $frombase the base of the input given
 * @param integer $tobase the base to convert to
 * @return string returns alphanumeric output representing a number of base n, where n = $tobase
 */
function my_base_convert($numstring, $frombase, $tobase) {
    $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $tostring = substr($chars, 0, $tobase);
    if (is_array($numstring)) {
        throw new \Exception('Trying to encrypt/decrypt a non-scalar value.');
    }
    $length = ($numstring) ? strlen($numstring) : 0;
    $result = '';
    for ($i = 0; $i < $length; $i++) {
        $number[$i] = strpos($chars, $numstring{$i});
    }
    do {
        $divide = 0;
        $newlen = 0;
        for ($i = 0; $i < $length; $i++) {
            $divide = $divide * $frombase + $number[$i];
            if ($divide >= $tobase) {
                $number[$newlen++] = (int)($divide / $tobase);
                $divide = $divide % $tobase;
            } elseif ($newlen > 0) {
                $number[$newlen++] = 0;
            }
        }
        $length = $newlen;
        $result = $tostring{$divide} . $result;
    }
    while ($newlen != 0);
    return $result;
}// function my_base_convert


/**
 * convert a string into HTML character entity references.
 * useful for preventing against email address harvesting spiders.<br>
 * do not confuse with {@link encrypt()}
 * <br><br>Example usage:
 * <code>
 * <a href="mailto:<?=encode('info@dom.com')?>"><?=encode('info@dom.com')?></a>
 * --
 * HTML source: &#105;&#110;&#102;&#111;&#64;&#100;&#111;&#109;&#46;&#99;&#111;&#109;
 * HTML output: info@dom.com
 * </code>
 * @param string $input
 * @return string returns a sequence of HTML character entity references
 */
function encode($input) {
    for ($i=0; $i<strlen($input); $i++) {
        $output .= '&#'.ord(substr($input,$i,1)).';';
    }//for
    return $output;
}//function



/**
 * Add ide's for all ids in the array
 *
 * @param array  $list         associative array
 * @param string $object_name  name of principal object (to use for 'id')
 * @return array
 */
function add_ides(array $list = [], $object_name = null ) {
    foreach ($list as $key => $value) {

        if (substr($key,-3) == '_id' && !$list[$key.'e']){
            $table = substr($key,0 ,-3);
            $list[$key.'e'] = encrypt($value, $table);




        }

    }

    if( $list['id'] && $object_name && !$list['ide']){
        $list['ide'] = encrypt($list['id'], $object_name);

    }

    return $list ;

}



function elapsed (){

}

function mem (){}

/**
 * Global functions
 */

/**
 * Clone all the fields from one object into another, both already initialized
 */
function clone_into($source, &$target)
{
    foreach ($source as $k => $v)
    {
        if (is_array($v))
        {
            $target->{$k} = array_to_object($v); //RECURSION
        }
        else
        {
            $target->{$k} = $v;
        }
    }
}

/**
 * Simple redirect
 *
 * @param string $url
 */
function redirect($url)
{
    header("Location:" . $url);
    die();
}


/**
 * Simple redirect
 *
 * @param string $url
 */
function redirectToAction($action)
{

    global $emagid;



    $route = \Emagid\Mvc\Mvc::$route;
    $route['action'] = $action;

    $url =\Emagid\Mvc\Url::action('index', $route) ;

    header("Location:" . $url);
    die();
}

/**
 * Global functions
 */
function startsWith($haystack, $needle)
{
    return $needle === "" || strpos($haystack, $needle) === 0;
}

function endsWith($haystack, $needle)
{
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

/**
 * convert an array to an object
 *
 * @param Array
 * @return strClass
 */
function array_to_object($array)
{

    return json_decode(json_encode($array));

    // $obj = new stdClass;
    // foreach($array as $k => $v) {
    //    if(is_array($v)) {
    //       $obj->{$k} = $this->array_to_object($v); //RECURSION
    //    } else {
    //       $obj->{$k} = $v;
    //    }
    // }
    // return $obj;
}

/**
 * convert object to an array
 *
 * @param $data Object
 * @return Array named array
 */
function object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {

            $result[$key] = object_to_array($value);
        }
        return $result;
    }
    return $data;
}

/**
 * checks whether an array is associative
 */
function is_assoc($var)
{
    return is_array($var) && array_diff_key($var, array_keys(array_keys($var)));
}


/**
 * flattens a multidimensional array
 */
function flatten(array $array) {
    $return = array();
    array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
    return $return;
}



/**
 * Makes sure $args is an array
 * If it isn't, return it as an array with key $key
 * Example Usage:
 *      function doSomething($params)
 *      {
 *          $params = arrayify($params, 'id');
 *      }
 * this would allow you to pass $params as '5'
 * instead of having to do: array('id' => 5), and both would work.
 * If no $key, wrapps $args in array
 *
 * @param   mixed   $args
 * @param   string  $key
 */
function arrayify($args, $key = null)
{
    return (!is_array($args))
        ? ( ($key) ? array($key => $args) : array($args) )
        : $args;
}



/**
 * Create a json output, send json headers and output the json.
 */
function exit_json($data){
    header("Content-Type: application/json");
    echo(json_encode($data));
    exit();
}


function exit_if_json($data){

    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')
    {
        exit_json($data);
    }

}