<?php
 if(count($model->users)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="20%">Username</th>
           <th width="20%">Email</th>
            <th width="20%">Name</th>
            <th width="10%">Type</th>
            <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th> 
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->users as $obj){ ?>
        <tr>
         <td>
         <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $obj->id; ?>">
           <?php echo $obj->username;?>
         </a>
      </td>
         <td>
         <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $obj->id; ?>">
           <?php echo $obj->email;?>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $obj->id; ?>">
           <?php echo $obj->full_name();?>
        </a>
      </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $obj->id; ?>">
                <?php echo $obj->wholesale_id?'Wholesale':'Customer';?>
            </a>
        </td>

         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>users/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>users/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'users';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>