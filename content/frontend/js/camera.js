$(document).ready(function(){


// ===========  CAMERA FUNCTION  ==============

    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

 
    function countDown() {
        var divs = $('.countdown');
        var timer
        var offset = 0 

        divs.each(function(){
            var self = this;
            timer = setTimeout(function(){
                $(self).css('font-size', '150px');
                $(self).fadeOut(1000);
            }, 1000 + offset);
            offset += 1000;
        });

        // flash
        setTimeout(function(){
            $('.flash').fadeIn(200);
            $('.flash').fadeOut(400);
            $(divs).css('font-size', '0px');
        }, 4000);
    }


    function gifPhotos() {
        var divs = $('.countdown');
        var offset = 1500

        for ( i=0; i<5; i++) {

            (function(i){
              setTimeout(function(){
                    $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='1200'></canvas></div>")
                    var canvas = $('.canvas');
                    var context = canvas[canvas.length -1].getContext('2d');
                    var video = document.getElementById('video');
                    var background = $('#background')[0];
                    var logo = $('.holiday')[0];
               

                    $('.flash').fadeIn(200);
                    $('.flash').fadeOut(400);

                    $(divs).css('font-size', '0px');
                   context.drawImage(background, 0, 0, 900, 1200);
                   context.drawImage(video, 130, 130, 640, 480);
                   context.drawImage(logo, 212, 730, 476, 400);
                   $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');

                   // watermark
                    // var img2 = new Image();
                    // img2.src = '/content/frontend/assets/img/logo_holder.png';
                    // context.drawImage(img2,530,420, 80, 43);

                    $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))
              
              }, offset);
              offset += 1500
            }(i));  
        }
    }


    function showGifs() {
        var photos = $('#pictures').children('.canvas_holder');
        $('#photos').hide();
        $('#pictures').fadeIn(1000);
        $('.gif_info').fadeIn();
        $('.home').css('position', 'relative');
        // debugger
        for ( i=0; i<6; i++ ) {
            $(photos[i]).css('display', 'inline-block');
        }
    }


    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    function convertCanvasToImage2(canvas) {
        var dataURL = canvas.toDataURL();
        return canvas.src = dataURL;
    }

    


    function showPictures() {
        $('#pictures').fadeIn(2000);
        $('.draw_ctl').fadeIn();
        var photos = $('#pictures').children('.photo');
            $(photos[0]).fadeIn();
            // var p = convertCanvasToImage($(photos[0]))
            // console.log('hi' + p)

        //$($(photos[0]).html()).attr('src');
        $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
            $('#submit_form input[name=image]').val(data.image.id);

        // $('.canvas_holder').append("<canvas id='draw' width='900' height='1200'></canvas>")
        //    context = document.getElementById('draw').getContext("2d");


        });

    }



    // GIF CLICK
    document.getElementById("snap_gif").addEventListener("click", function() {
        $(this).fadeOut(1000)
        $(this).css('pointer-events', 'none');
        var pics = []

        countDown();
        setTimeout(function(){
            // for testing
            $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='1200'></canvas></div>")
            var canvas = $('.canvas');
            var context = canvas[canvas.length -1].getContext('2d');
            var video = document.getElementById('video');
            var background = $('#background')[0];
            var logo = $('.holiday')[0];
               context.drawImage(background, 0, 0, 900, 1200);
               context.drawImage(video, 130, 130, 640, 480);
               context.drawImage(logo, 212, 730, 476, 400);

           $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');


            $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


           gifPhotos();

        }, 4000 )

        setTimeout(function(){
            $('#photos').css('opacity', '0');
            setTimeout(function(){
                $('#photos').fadeOut();
            }, 1100);

            showGifs();
            $(this).css('pointer-events', 'all');
        }, 10000);
    });



    // PHOTO CLICK
    document.getElementById("snap_photo").addEventListener("click", function() {
        $(this).fadeOut(1000);
        $(this).css('pointer-events', 'none');
        var pics = []

        countDown();
            // take the picture
            setTimeout(function(){
                $('#pictures').append("<div class='canvas_holder photo'><canvas class='canvas' width='900' height='1200'></canvas></div>")
                
                var canvas = $('.canvas');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
                var logo = $('.holiday')[0];
               context.drawImage(background, 0, 0, 900, 1200);
               context.drawImage(video, 130, 130, 640, 480);
               context.drawImage(logo, 212, 730, 476, 400);
               

                $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))



            }, 4000);

        setTimeout(function(){
            $('#photos').slideUp()
            $('.submit').delay(1000).fadeIn();
            $('.print').delay(1000).fadeIn();
            $(this).css('pointer-events', 'all');
            showPictures();
        }, 5000);
    });



    // DRAWING
    // $(document).on('touchstart', '#draw', function(e){
    //   var mouseX = e.pageX - this.offsetLeft;
    //   var mouseY = e.pageY - this.offsetTop;
    //   console.log(e)

            
    //   paint = true;
    //   addClick(e.clientX - this.offsetLeft, e.clientY - this.offsetTop);
    //   redraw();
    // });

    //     $(document).on('mousedown', '#draw', function(e){
    //   var mouseX = e.pageX - this.offsetLeft;
    //   var mouseY = e.pageY - this.offsetTop;
    //   console.log(e)
            
    //   paint = true;
    //   addClick(e.clientX - this.offsetLeft, e.clientY - this.offsetTop);
    //   redraw();
    // });

    // $(document).on('touchmove', '#draw', function(e){
    //     paint = true
    //   if(paint){
    //     addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
    //     redraw();
    //   }
    // });

    // $(document).on('mouseup', '#draw', function(e){
    //   paint = false;
    // });

    // $(document).on('mouseleave', '#draw', function(e){
    //   paint = false;
    // });

    // var clickX = new Array();
    // var clickY = new Array();
    // var clickDrag = new Array();
    // var paint;

    // function addClick(x, y, dragging)
    // {
    //   clickX.push(x);
    //   clickY.push(y);
    //   clickDrag.push(dragging);
    // }

    // function redraw(){
    //   context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
      
    //   context.strokeStyle = "#df4b26";
    //   context.lineJoin = "round";
    //   context.lineWidth = 5;
                
    //   for(var i=0; i < clickX.length; i++) {        
    //     context.beginPath();
    //     if(clickDrag[i] && i){
    //       context.moveTo(clickX[i-1], clickY[i-1]);
    //      }else{
    //        context.moveTo(clickX[i]-1, clickY[i]);
    //      }
    //      context.lineTo(clickX[i], clickY[i]);
    //      context.closePath();
    //      context.stroke();
    //   }
    // }


    // $('.draw_ctl button').click(function(){
    //   clickX = new Array();
    //   clickY = new Array();
    //   clickDrag = new Array();
    //     context.clearRect(0, 0, 900, 1200);

    // });


});



